# argo notifications configuration

```
  service.webhook.gitlab: |
    url: https://gitlab.com/api/v4/projects/42866006/ref/container-image/trigger/pipeline?token=TOKEN_HERE
    insecureSkipVerify: true
  template.gitlab-pipeline: |
    webhook:
      gitlab:
        method: POST
  trigger.on-success: |
    - when: app.status.operationState.phase == 'Succeeded' and app.status.health.status == 'Healthy'
      send: [gitlab-pipeline]
      oncePer: app.status.sync.revision
```